package de.kk.estcalculator.view.encyclopedia;

import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.tree.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;

public class EncyclopediaView {

    private JPanel contentPane;
    private JTree trBrowser;
    private JEditorPane epHtmlViewer;

    public EncyclopediaView() {
        createTree();

        JFrame testFrame = new JFrame("TestFrame");

        Dimension testFrameSize = new Dimension(600, 500);
        testFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        testFrame.setMinimumSize(testFrameSize);

        testFrame.add(contentPane);
        testFrame.pack();

        testFrame.setVisible(true);
    }

    public void createTree() {
        DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode("Encyclopedia");

        URL rootFolderLocation = getClass().getResource("/resources/encyclopedia");

        try {
            File rootFolder = new File(rootFolderLocation.toURI());

            createNodes(rootNode, rootFolder);
        }catch(URISyntaxException exception) {
            epHtmlViewer.setText("Fehler beim laden der Enzyklopädie!");
        }

        DefaultTreeModel model = new DefaultTreeModel(rootNode);
        trBrowser.setModel(model);

        trBrowser.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);

        /*
        trBrowser.addTreeSelectionListener((TreeSelectionEvent event) -> {

            Thread t = new Thread(() -> {
                try {

                    loadHTML(getClass().getResource(String.format("/resources/encyclopedia/%s.html", trBrowser.getLastSelectedPathComponent().toString())));

                }catch(Exception e) {
                    System.out.println("Fehler im Thread");
                }
            });

            t.start();

        });
        */
    }

    public void createNodes(DefaultMutableTreeNode rootNode, File rootFile) {
        for(File file : rootFile.listFiles()) {
            String fileName = file.getName();
            boolean isDirectory = file.isDirectory();

            DefaultMutableTreeNode node = null;

            if(isDirectory) {
                node = new DefaultMutableTreeNode(fileName);
                rootNode.add(node);

                createNodes(node, file);
            }else {
                if(fileName.matches(".*.html") == false)
                    continue;

                if(fileName.length() < 6)
                    continue;

                node = new DefaultMutableTreeNode(fileName.substring(0, fileName.length() - 5));
                rootNode.add(node);
            }
        }
    }

    public void loadHTML(URL url) {
        if(url == null)
            return;

        try {
            InputStream streamIN = url.openStream();
            InputStreamReader iSReader = new InputStreamReader(streamIN);
            BufferedReader bReader = new BufferedReader(iSReader);

            String line;
            StringBuilder stringBuilder = new StringBuilder();

            while((line = bReader.readLine()) != null) {
                stringBuilder.append(line);
            }

            iSReader.close();
            bReader.close();

            SwingUtilities.invokeLater(() -> {
                epHtmlViewer.setText(stringBuilder.toString());
            });

        }catch(Exception e) {
            System.out.println("Fehler beim Laden der .html:");
            e.printStackTrace();
        }
    }

}
