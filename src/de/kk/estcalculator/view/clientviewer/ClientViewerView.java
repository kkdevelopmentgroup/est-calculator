package de.kk.estcalculator.view.clientviewer;

import de.kk.estcalculator.data.ClientDatabase;
import de.kk.estcalculator.model.client.Client;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class ClientViewerView {

    private JPanel contentPane;
    private JButton btnNew;
    private JButton btnOpen;
    private JButton btnEdit;
    private JTable tblClients;

    private DefaultTableModel tableModel;

    public ClientViewerView() {

        tableModel = new DefaultTableModel(new String[] {"Name", "Straße & Hausnr.", "PLZ & Stadt", "Telefonnr."}, 0);
        tblClients.setModel(tableModel);

    }

    public JPanel getContentPane() {
        return contentPane;
    }

    public void addClient(Client client) {
        tableModel.addRow(new Object[] {client.getName(), client.getAdress(), client.getZipAndCity(), client.getPhoneNumber()});
    }

    public void loadClients() {
        for(Client c : ClientDatabase.getClients())
            addClient(c);
    }

}
