package de.kk.estcalculator.view.clientviewer.createclient;

import de.kk.estcalculator.view.clientviewer.createclient.listener.BtnOKActionListener;

import javax.swing.*;

public class CreateClientView extends JFrame {

    private JPanel contentPane;
    private JTextField tfName;
    private JTextField tfStreet;
    private JTextField tfCity;
    private JTextField tfTelefonnumber;
    private JButton btnOK;
    private JButton btnCancel;

    public CreateClientView() {
        super("Mandanten anlegen...");

        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        setContentPane(contentPane);

        pack();

        btnOK.addActionListener(new BtnOKActionListener(this));
    }

    public JTextField getTfName() {
        return tfName;
    }

    public JTextField getTfStreet() {
        return tfStreet;
    }

    public JTextField getTfCity() {
        return tfCity;
    }

    public JTextField getTfTelefonnumber() {
        return tfTelefonnumber;
    }

}
