package de.kk.estcalculator.view.clientviewer.createclient.listener;

import de.kk.estcalculator.data.ClientDatabase;
import de.kk.estcalculator.model.client.Client;
import de.kk.estcalculator.view.clientviewer.createclient.CreateClientView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class BtnOKActionListener implements ActionListener {

    private CreateClientView createClientView;

    public BtnOKActionListener(CreateClientView createClientView) {
        this.createClientView = createClientView;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String name = createClientView.getTfName().getText();
        String address = createClientView.getTfStreet().getText();
        String city = createClientView.getTfCity().getText();
        String phoneNumber = createClientView.getTfTelefonnumber().getText();

        if(name.matches("\\D+") && address.matches("\\D+\\s\\d+") && city.matches("\\d{5}\\s\\D+") && phoneNumber.matches("\\d+")) {
            Client c = new Client(name, address, city, phoneNumber);
            ClientDatabase.getClients().add(c);

            createClientView.setVisible(false);
            createClientView.dispose();
        }else {
            JOptionPane.showMessageDialog(createClientView.getContentPane(), "Die Eingaben haben ein falsches Format!", "Fehler", JOptionPane.ERROR_MESSAGE);
        }
    }

}
