package de.kk.estcalculator.view.title;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class TitleView {

    private JPanel contentPane;

    public TitleView() {
        contentPane.add(new JImageView());
    }

    public class JImageView extends JComponent {

        private static final long serialVersionUID = 1L;

        private BufferedImage backgroundImage;

        JImageView() {
            setPreferredSize(new Dimension(324, 155));

            try {
                backgroundImage = ImageIO.read(getClass().getResource("/de/kk/estcalculator/view/title/background.jpg"));
            }catch(IOException e) {
                setBackground(Color.BLUE);
            }
        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);

            if(backgroundImage != null)
                g.drawImage(backgroundImage, 0, 0, this);
        }

    }

}
