package de.kk.estcalculator;

import de.kk.estcalculator.data.ClientDatabase;
import de.kk.estcalculator.model.client.Client;
import de.kk.estcalculator.view.clientviewer.ClientViewerView;
import de.kk.estcalculator.view.clientviewer.createclient.CreateClientView;

import javax.swing.*;

public class Main {

    public static void main(String[] args) {
        //DEBUG

        //ClientDatabase.readClients();

        JFrame test = new JFrame("Test Frame!");
        test.setResizable(true);
        test.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        //ADD COMPONENTS

        //ClientViewerView clientView = new ClientViewerView();
        //clientView.loadClients();

        CreateClientView createClientView = new CreateClientView();
        createClientView.setVisible(true);

        //ADD COMPONENTS

        test.pack();
        //test.setVisible(true);

        //DEBUG
    }

}
