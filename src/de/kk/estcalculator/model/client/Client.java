package de.kk.estcalculator.model.client;

import java.io.Serializable;

public class Client implements Serializable {

    private String name;
    private String adress;
    private String zipAndCity;
    private String phoneNumber;

    public Client(String name, String adress, String zipAndCity, String phoneNumber) {
        this.name = name;
        this.adress = adress;
        this.zipAndCity = zipAndCity;
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getZipAndCity() {
        return zipAndCity;
    }

    public void setZipAndCity(String zipAndCity) {
        this.zipAndCity = zipAndCity;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

}
