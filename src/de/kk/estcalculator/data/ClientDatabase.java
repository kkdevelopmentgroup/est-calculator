package de.kk.estcalculator.data;

import de.kk.estcalculator.model.client.Client;

import java.io.*;
import java.util.ArrayList;

public class ClientDatabase {

    private static ArrayList<Client> clients = new ArrayList<>();

    public static void saveClients() {
        Thread worker = new Thread(() -> {
            try {
                FileOutputStream fos = new FileOutputStream("Clients.kk");
                ObjectOutputStream oos = new ObjectOutputStream(fos);

                oos.writeObject(clients);

                oos.close();
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        worker.start();
    }

    public static void readClients() {
        Thread worker = new Thread(() -> {
            try {
                FileInputStream fis = new FileInputStream("Clients.kk");
                ObjectInputStream ois = new ObjectInputStream(fis);

                clients = (ArrayList<Client>) ois.readObject();

                ois.close();
                fis.close();

            } catch (Exception e) {
            }
        });

        worker.start();
    }

    public static ArrayList<Client> getClients() {
        return clients;
    }

}
